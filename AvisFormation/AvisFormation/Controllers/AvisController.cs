﻿using AvisFormation.DATA;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AvisFormation.Controllers
{
    [Authorize]
    public class AvisController : Controller
    {
        private AvisFormationEntities db = new AvisFormationEntities();
        private int _formationId;
        // GET: Avis
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create(int id)
        {
            ViewBag.Nom = db.Formation.Where(f => f.Id == id).FirstOrDefault().Nom;
            ViewBag.IdFormation = id;
            var avis = new Avis();
            return View(avis);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( Avis avis)
        {
            if (ModelState.IsValid)
            {

                avis.UserId= User.Identity.GetUserId();
                avis.Nom = User.Identity.GetUserName();
                avis.DateAvis = DateTime.Now;
                db.Avis.Add(avis);
                db.SaveChanges();
                return RedirectToAction("Index", "Formation");
            }
           
            ViewBag.IdFormation = db.Formation.Where(f => f.Id == avis.IdFormation).FirstOrDefault().Nom;
            return View(avis);
        }
    }
}