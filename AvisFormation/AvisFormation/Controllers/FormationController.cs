﻿using AvisFormation.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AvisFormation.Controllers
{
    public class FormationController : Controller
    {
        private AvisFormationEntities dbFormations = new AvisFormationEntities();

        public ActionResult Index()
        {
            var lstFormations = dbFormations.Formation.ToList();
            return View(lstFormations);
        }
    }
}